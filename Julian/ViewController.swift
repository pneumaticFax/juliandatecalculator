//
//  ViewController.swift
//  Julian
//
//  Created by Durandal on 8/12/17.
//  Copyright © 2017 pneumaticFax. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func showMessage(){
        
        let date = Date()
        let cal = Calendar.current
        let day = cal.ordinality(of: .day, in: .year, for: date)
        
        
        let alertController = UIAlertController(title: "Julian Date Converter",
                                                message: "The Julian Day is: \(day!)",
                                                preferredStyle: UIAlertControllerStyle.alert)
        
        alertController.addAction(UIAlertAction(title: "OK",
                                                style: UIAlertActionStyle.default,
                                                handler: nil))
        
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func showMessage2(){
        let alertController = UIAlertController(title: "Julian Date Converter",
                                                message: "No",
                                                preferredStyle: UIAlertControllerStyle.alert)
        
        alertController.addAction(UIAlertAction(title: "OK",
                                                style: UIAlertActionStyle.default,
                                                handler: nil))
        
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func showMessage3(){
        let alertController = UIAlertController(title: "Julian Date Converter",
                                                message: "who?",
                                                preferredStyle: UIAlertControllerStyle.alert)
        
        alertController.addAction(UIAlertAction(title: "OK",
                                                style: UIAlertActionStyle.default,
                                                handler: nil))
        
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func showMessage4(){
        let alertController = UIAlertController(title: "Julian Date Converter",
                                                message: "wat...",
                                                preferredStyle: UIAlertControllerStyle.alert)
        
        alertController.addAction(UIAlertAction(title: "OK",
                                                style: UIAlertActionStyle.default,
                                                handler: nil))
        
        present(alertController, animated: true, completion: nil)
    }
}

